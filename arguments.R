
arguments$public_dir_path <- "U:/Marketing/BI & Analytics/Shared/DailyMarketReport/"

arguments$mmMarketsDF <- as.data.frame(matrix(
        c("Denmark",        "ga:107277301",
          "Finland",        "ga:107259862",
          "Norway",         "ga:107268754",
          "Sweden",         "ga:107262065",
          "Australia",      "ga:114289178", 
          "Austria",        "ga:107256268",
          "France",         "ga:107276009",
          "Germany",        "ga:114286948",
          "Italy",          "ga:107263961",
          "Netherlands",    "ga:107276105",
          "Portugal",       "ga:107260263",
          "Russia",         "ga:107257781",
          "Spain",          "ga:107260259",
          "Switzerland",    "ga:107273611",
          "United Kingdom", "ga:107260246",
          "United States",  "ga:107260478",
          "Belgium",        "ga:107260743",
          "Brazil",         "ga:107254497",
          "Canada",         "ga:107268243",
          "China",          "ga:107277003",
          "Czech Republic", "ga:107270144",
          "Hong Kong",      "ga:107268357",
          "India",          "ga:107277500",
          "Ireland",        "ga:107258682",
          "Mexico",         "ga:107274819",
          "New Zealand",    "ga:107274800",
          "Poland",         "ga:107272830",
          "Romania",        "ga:107263074",
          "South Africa",   "ga:107271643",
          "Taiwan",         "ga:107276804",
          "Turkey",         "ga:107268264",
          "Ukraine",        "ga:107260768"#, "Global",         "ga:119459360"
        ),
        ncol = 2,
        byrow = TRUE),
        stringsAsFactors = FALSE)
colnames(arguments$mmMarketsDF) <- c("market", "gaView")



arguments$cfMarketsDF <- as.data.frame(matrix(
        c("Canada",         "ga:107254289",
          "Indonesia",      "ga:107258370",
          "New Zealand",    "ga:107261242",
          "UK",             "ga:107268136",
          "South Africa",   "ga:107270026",
          "Australia",      "ga:107269935",
          "Hong Kong",      "ga:107254292",
          "Malaysia",       "ga:107258373",
          "Nigeria",        "ga:107271222",
          "Philippines",    "ga:107262847",
          "Singapore",      "ga:107254998",
          "US",             "ga:107254090",
          "Qatar",          "ga:124852549"
        ),
        ncol = 2,
        byrow = TRUE),
        stringsAsFactors = FALSE)
colnames(arguments$cfMarketsDF) <- c("market", "gaView")