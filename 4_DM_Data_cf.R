#---- Settings and inits ----
require(httr)
require(RGoogleAnalytics)

dataFile <- paste0(arguments$project_path, "Data/", arguments$brand, "-gaDataMaster.Rda")
if(file.exists(dataFile)) {
        load(dataFile)
        latestData <- max(gaDataMaster$date)
        #firstData <- min(gaDataMaster$date)
} else {
        latestData <- "2000-01-01"
}
rm(dataFile)

endDate <- Sys.Date() - 1
#endDate <- firstData - 1
startDate <- endDate - 13
threashold_loss <- (0-9.1)
threashold_gain <- -threashold_loss
update_data <- TRUE #can be set to false for testing purposes, sets to false if latestData == endDate
update_data <- ifelse((as.Date(latestData) == endDate), FALSE, TRUE)
#marketsTestMode <- FALSE
#if(marketsTestMode) { marketsDF <- marketsDF[1:5, ] }

# Markets to screen
marketsDF <-    if(arguments$brand == "mm") { arguments$mmMarketsDF } else if(arguments$brand == "cf") { arguments$cfMarketsDF }

print(paste("GaDataMaster loaded with latest-date: ", latestData, ". Markets loaded: ", nrow(marketsDF)))

# Authorize the Google Analytics account
# if token file exists
if(file.exists(paste0(arguments$project_path, "/token_file"))) {
        load(file=paste0(arguments$project_path, "token_file")) #load file
        ValidateToken(token)
        
} else { #if token file does not exist - make and save
        #auth file should contain relevant client.id and client.secret
        source(file = paste0(arguments$project_path, "auth.R"))
        
        token <- Auth(client.id = client.id, 
                      client.secret = client.secret)
        
        save(token, file=paste0(arguments$project_path, "/token_file"))
        
        print("Token created and saved")
        
        rm(client.id, client.secret)
}

#---- Get Data from Google ----

# Get Data from Google Analytics, if set to update
if(update_data) {
        began <- Sys.time()
        print(paste("Data call started at ", began))
        
        if(latestData>=startDate) {
                queryStartDate <- as.Date(latestData) + 1 
        } else {
                queryStartDate <- startDate
        }
        
        if(queryStartDate==endDate) {
                queryStartDate <- endDate - 1 
        }
        queryEndDate <- endDate
        
        
        for (market in marketsDF$market) {
                #market <- marketsDF$market[1]
                # Just to let us know what is going on
                print(paste("Getting: ", market))
                
                # Get the propertyID
                profileId <- marketsDF[marketsDF$market == market, ]$gaView
                
                
                # Build a list of all the Query Parameters
                query.list <- Init(start.date = paste0(queryStartDate),
                                   end.date =   paste0(queryEndDate),
                                   dimensions = c("ga:date", "ga:channelGrouping"),
                                   metrics = c("ga:transactionRevenue", "ga:sessions", "ga:transactions", "ga:metric1"),
                                   max.results = 10000,
                                   sort = "ga:date",
                                   table.id = profileId)
                ga.query <- QueryBuilder(query.list)         # Create the Query Builder object so that the query parameters are validated
                tmp_gaData1 <- GetReportData(ga.query, token, split_daywise = T)        # Extract the data and store it in a data-frame

                # We manually add the country/market
                tmp_gaData1 <- cbind(market = market, tmp_gaData1)
                
                # Just to let us know what is going on
                print(paste("Getting: ", market, " - data2"))
                
                # Then we wish to split by product (RedirectType)
                # Build a list of all the Query Parameters
                query.list <- Init(start.date = paste0(queryStartDate),
                                   end.date =   paste0(queryEndDate),
                                   dimensions = c("ga:date", "ga:channelGrouping", "ga:dimension41"),
                                   metrics = c("ga:transactionRevenue", "ga:transactions"),
                                   max.results = 10000,
                                   sort = "ga:date",
                                   table.id = profileId)
                ga.query <- QueryBuilder(query.list)         # Create the Query Builder object so that the query parameters are validated
                tmp_gaData2 <- GetReportData(ga.query, token, split_daywise = T)        # Extract the data and store it in a data-frame
                
                # We manually add the country/market
                tmp_gaData2 <- cbind(market = market, tmp_gaData2)
                
                # stacking the retrieved data
                if(exists("gaData1")) {
                        gaData1 <- rbind(gaData1, tmp_gaData1)
                        gaData2 <- rbind(gaData2, tmp_gaData2)
                        print(paste("Collected: ", nrow(tmp_gaData1), " new rows in data 1, and ", nrow(tmp_gaData2), " new rows in data 2."))
                } else {
                        gaData1 <- tmp_gaData1
                        gaData2 <- tmp_gaData2
                        print(paste("First market data gathered: ", nrow(tmp_gaData1), " new rows in data 1, and ", nrow(tmp_gaData2), " new rows in data 2."))
                }
                
                # Cleaning up
                rm(tmp_gaData1, tmp_gaData2)
        }
        
        ended <- Sys.time()
        print(paste("Data call ended at ", ended))
        print(ended - began)
        
        rm(began, ended)
} 

rm(ga.query, profileId, query.list, queryEndDate, queryStartDate)

#---- Sort, join and load data ----

# if update is true, we bind newly queried data to old data.
# update will render false, if we have already queried data. But then we will just load latest dump (see the ELSE)
if(update_data) {
  # make variable for later join
        gaData1$joinBy <- paste(gaData1$date, gaData1$market, gaData1$channelGrouping, sep="-")
  gaData2$joinBy <- paste(gaData2$date, gaData2$market, gaData2$channelGrouping, sep="-")

  # make flight rev and transact.
  gaDataFlight                    <- gaData2[gaData2$dimension41 == "flight", ]
  gaDataFlight$flightRev          <- gaDataFlight$transactionRevenue
  gaDataFlight$flightTransactions <- gaDataFlight$transactions
  gaDataFlight                    <- gaDataFlight[ , c("joinBy", "flightRev", "flightTransactions")]

  # make hotel rev and transact.
  gaDataOther                   <- gaData2[gaData2$dimension41 != "flight", ]
  gaDataOther$otherRev          <- gaDataOther$transactionRevenue
  gaDataOther$otherTransactions <- gaDataOther$transactions
  gaDataOther                   <- gaDataOther[ , c("joinBy", "otherRev", "otherTransactions")]
  gaDataOther                   <- aggregate(. ~ joinBy, data=gaDataOther, FUN=sum)

  # Join and clean up NA's
  gaData0 <- merge(x = gaData1, y = gaDataFlight, by = "joinBy", all.x = TRUE)
  gaData0 <- merge(x = gaData0, y = gaDataOther, by = "joinBy", all.x = TRUE)
  gaData0 [is.na(gaData0)] <- 0
  gaData0 <- gaData0[order(gaData0$date), ]
  gaData0 <- gaData0[order(gaData0$market), ]

  # Make conversion rates
  gaData0$conversionRate <- gaData0$transactions / gaData0$sessions
  gaData0$conversionRateFlight <- gaData0$flightTransactions / gaData0$sessions
  gaData0$conversionRateOther <- gaData0$otherTransactions / gaData0$sessions
  gaData0 <- subset(gaData0, select = -joinBy )
  
  gaData0 <- gaData0[order(gaData0$channelGrouping), ]
  gaData0 <- gaData0[order(gaData0$market), ]
  gaData0 <- gaData0[order(gaData0$date), ]
  names(gaData0)[names(gaData0) == 'metric1'] <- 'flightSearches'
  gaData0$date <- as.Date(format(paste0(substr(gaData0$date, 1, 4), "-",
                                        substr(gaData0$date, 5, 6), "-",
                                        substr(gaData0$date, 7, 8)),
                                 usetz = FALSE))
  
  if(exists("gaDataMaster")) {
          gaDataMaster <- gaDataMaster[order(gaDataMaster$channelGrouping), ]
          gaDataMaster <- gaDataMaster[order(gaDataMaster$market), ]
          gaDataMaster <- gaDataMaster[order(gaDataMaster$date), ]
          gaDataMaster <- rbind(gaDataMaster[gaDataMaster$date < min(gaData0$date), ], gaData0)
  } else {
          gaDataMaster <- gaData0
  }
  gaDataMaster <- gaDataMaster[order(gaDataMaster$channelGrouping), ]
  gaDataMaster <- gaDataMaster[order(gaDataMaster$market), ]
  gaDataMaster <- gaDataMaster[order(gaDataMaster$date), ]
  # Uniqueness test
  if(TRUE) {
    uni0 <- paste(gaDataMaster$market, gaDataMaster$date, gaDataMaster$channelGrouping, sep="")
    if(length(unique(uni0)) == length(uni0)) {
      print(paste("No duplicates found"))
    } else {
      print(paste("Warning: there might be duplicates in dataset. Deleting duplicates."))
      gaDataMaster$uni <- paste(gaDataMaster$market, gaDataMaster$date, gaDataMaster$channelGrouping, sep="")
      gaDataMaster <- gaDataMaster[order(gaDataMaster$uni), ]
      gaDataMaster <- gaDataMaster[!duplicated(gaDataMaster), ]
    }
    rm(uni0)
  }
  
  # Reset rownames
  rownames(gaDataMaster) <- seq(length=nrow(gaDataMaster)) 
  print(paste("gaDataMaster has ", nrow(gaDataMaster), " rows"))
  
  save(gaDataMaster, file = paste(arguments$project_path, "Data/", arguments$brand, "-gaDataMaster.Rda", sep=""))
  save(gaDataMaster, file = paste(arguments$project_path, "Data/", arguments$brand, "-gaDataMaster-", Sys.Date(), ".Rda", sep=""))
  gaData <- gaDataMaster[gaDataMaster$date >= startDate, ]
  
  save(gaData, file = paste(arguments$project_path, "Data/", arguments$brand, "-gaData-", Sys.Date(), ".Rda", sep=""))
  save(gaData, file = paste(arguments$project_path, "Data/", arguments$brand, "-gaData", ".Rda", sep=""))
  # Clean up
  rm(gaData0, gaData1, gaData2, gaDataFlight, gaDataOther, gaDataMaster)
  
} else {
        load(paste(arguments$project_path, "Data/", arguments$brand, "-gaData.Rda", sep=""))
        rm(gaDataMaster)
        print("Data loaded from file")
}

# For testing
#library(xlsx)
#write.xlsx(gaData, "gaData.xlsx", sheetName = "1")




#---- function: getEffect ----

getEffect <- function(market, channel, product, gaDataSet, dates = NA) {
  
  if(product=="allProducts") {
    revenueName <- "transactionRevenue"
    searchName <- c("flightSearches")
    transactionsName <- "transactions"
  } else if(product=="flight") {
    revenueName <- "flightRev"
    searchName <- "flightSearches"
    transactionsName <- "flightTransactions"
  } else if(product=="other") {
    revenueName <- "otherRev"
    searchName <- "otherSearches"
    transactionsName <- "otherTransactions"
  }

  if(!is.na(dates)) {
    periodEnd <- dates[1]
    periodStart <- dates[2]
    period_1End <- dates[3]
    period_1Start <- dates[4]
  } else {
    periodEnd <- endDate
    periodStart <- (endDate-6)
    period_1End <- (endDate-7)
    period_1Start <- (endDate-13)   
  }
    
  # Subset dataset by market
  tmp <- gaDataSet[gaDataSet$market == market, ]
  
  # and by channel
  if(channel != "allChannels") {
    tmp <- tmp[tmp$channelGrouping == channel, ]
  }
  
  # and by dates
  tmp <- rbind(tmp[tmp$date >= periodStart & tmp$date <= periodEnd, ],
               tmp[tmp$date >= period_1Start & tmp$date <= period_1End, ])
  
  
  cols <- c("revWeek", "revWeek_1", "revDifWvW", "revWoW", "sesWeek", "sesWeek_1", "sesWoW", "serWeek",
            "serWeek_1", "serWoW", "trnWeek", "trnWeek_1", "trnWoW", "spsWeek", "spsWeek_1", "spsWvW", "tpsWeek",
            "tpsWeek_1", "tpsWvW", "rptWeek", "rptWeek_1", "rptWvW", "conWeek", "conWeek_1", "conWvW", "dSesPct",
            "dConPct", "dRptPct", "dSpsPct", "dTpsPct", "dSes", "dCon", "dRpt", "dSps", "dTps", "dSesPctWoW",
            "dConPctWoW", "dRptPctWoW", "dSpsPctWoW", "dTpsPctWoW", "flag")
  
  dfOut <- data.frame(matrix(0, ncol = length(cols), nrow = 1))
  colnames(dfOut) <- cols
  dfOut <- cbind(market = market, product = product, channel = channel, dfOut)

  # make container for output
  output <- list()

  revWeek <- sum(tmp[tmp$date >= periodStart & tmp$date <= periodEnd, revenueName])
  revWeek_1 <- sum(tmp[tmp$date >= period_1Start & tmp$date <= period_1End, revenueName])
  
  # Filters small observations out
  if(sum(tmp[ , revenueName]) < 20) {
    dfOut$flag <- 1
    output[[length(output)+1]] <- list(1, paste("Revenue less than 20 GBP. Returning zeros."))
    output[[length(output)+1]] <- dfOut
  } else {
    # Revenue sums
    dfOut$revWeek <- sum(tmp[tmp$date >= periodStart & tmp$date <= periodEnd, revenueName])
    dfOut$revWeek_1 <- sum(tmp[tmp$date >= period_1Start & tmp$date <= period_1End, revenueName])
    # Revenue difference, week vs week
    dfOut$revDifWvW <- dfOut$revWeek - dfOut$revWeek_1
    # Revenue week over week
    dfOut$revWoW <- dfOut$revWeek / dfOut$revWeek_1 - 1
    
    # Sessions
    dfOut$sesWeek <- sum(tmp[tmp$date >= periodStart & tmp$date <= periodEnd,]$sessions)
    dfOut$sesWeek_1 <- sum(tmp[tmp$date >= period_1Start & tmp$date <= period_1End,]$sessions)
    dfOut$sesWoW <- dfOut$sesWeek / dfOut$sesWeek_1 - 1
    
    # Total searches
    if(product=="other") {
      dfOut$serWeek <- 0
      dfOut$serWeek_1 <- 0
      dfOut$serWoW <- 0
    } else {
      dfOut$serWeek <- sum(tmp[tmp$date >= periodStart & tmp$date <= periodEnd, searchName])
      dfOut$serWeek_1 <- sum(tmp[tmp$date >= period_1Start & tmp$date <= period_1End, searchName])
      dfOut$serWoW <- dfOut$serWeek / dfOut$serWeek_1 - 1
    }
    
    # Transactions
    dfOut$trnWeek <- sum(tmp[tmp$date >= periodStart & tmp$date <= periodEnd, transactionsName])
    dfOut$trnWeek_1 <- sum(tmp[tmp$date >= period_1Start & tmp$date <= period_1End, transactionsName])
    dfOut$trnWoW <- dfOut$trnWeek / dfOut$trnWeek_1 - 1
    
    # Search per sess
    if(product=="other") {
      dfOut$spsWeek <- 0
      dfOut$spsWeek_1 <- 0
      dfOut$spsWvW <- 0
    } else {
      dfOut$spsWeek <- dfOut$serWeek / dfOut$sesWeek
      dfOut$spsWeek_1 <- dfOut$serWeek_1 / dfOut$sesWeek_1
      dfOut$spsWvW <- dfOut$spsWeek - dfOut$spsWeek_1
    }
    
    # Trans per search
    if(product=="other") {
      dfOut$tpsWeek <- 0
      dfOut$tpsWeek_1 <- 0
      dfOut$tpsWvW <- 0
    } else {
      dfOut$tpsWeek <- dfOut$trnWeek / dfOut$serWeek
      dfOut$tpsWeek_1 <- dfOut$trnWeek_1 / dfOut$serWeek_1
      dfOut$tpsWvW <- dfOut$tpsWeek - dfOut$tpsWeek_1
    }
    
    # Rev per transaction
    dfOut$rptWeek <- dfOut$revWeek / dfOut$trnWeek
    dfOut$rptWeek_1 <- dfOut$revWeek_1 / dfOut$trnWeek_1
    dfOut$rptWvW <- dfOut$rptWeek - dfOut$rptWeek_1
    
    # Conversion Rate
    dfOut$conWeek <- dfOut$trnWeek/dfOut$sesWeek
    dfOut$conWeek_1 <- dfOut$trnWeek_1/dfOut$sesWeek_1
    dfOut$conWvW <- dfOut$trnWeek/dfOut$sesWeek - dfOut$trnWeek_1/dfOut$sesWeek_1
    
    # attribute change to components
    # backward
    dSesB  <- (dfOut$sesWeek-dfOut$sesWeek_1)*dfOut$conWeek*dfOut$rptWeek
    dConB  <- dfOut$sesWeek*(dfOut$conWeek-dfOut$conWeek_1)*dfOut$rptWeek
    dRptB  <- dfOut$sesWeek*dfOut$conWeek*(dfOut$rptWeek-dfOut$rptWeek_1)
    
    dSes2B <- (dfOut$sesWeek-dfOut$sesWeek_1)*dfOut$spsWeek*dfOut$tpsWeek*dfOut$rptWeek
    dSpsB  <- ifelse(product=="other", 0, dfOut$sesWeek*(dfOut$spsWeek-dfOut$spsWeek_1)*dfOut$tpsWeek*dfOut$rptWeek)
    dTpsB  <- ifelse(product=="other", 0, dfOut$sesWeek*dfOut$spsWeek*(dfOut$tpsWeek-dfOut$tpsWeek_1)*dfOut$rptWeek)
    dRpt2B <- dfOut$sesWeek*dfOut$spsWeek*dfOut$tpsWeek*(dfOut$rptWeek-dfOut$rptWeek_1)
    
    #Forward
    dSesF  <- (dfOut$sesWeek-dfOut$sesWeek_1)*dfOut$conWeek_1*dfOut$rptWeek_1
    dConF  <- dfOut$sesWeek_1*(dfOut$conWeek-dfOut$conWeek_1)*dfOut$rptWeek_1
    dRptF  <- dfOut$sesWeek_1*dfOut$conWeek_1*(dfOut$rptWeek-dfOut$rptWeek_1)
    
    dSes2F <- (dfOut$sesWeek-dfOut$sesWeek_1)*dfOut$spsWeek_1*dfOut$tpsWeek_1*dfOut$rptWeek_1
    dSpsF  <- ifelse(product=="other", 0, dfOut$sesWeek_1*(dfOut$spsWeek-dfOut$spsWeek_1)*dfOut$tpsWeek_1*dfOut$rptWeek_1)
    dTpsF  <- ifelse(product=="other", 0, dfOut$sesWeek_1*dfOut$spsWeek_1*(dfOut$tpsWeek-dfOut$tpsWeek_1)*dfOut$rptWeek_1)
    dRpt2F <- dfOut$sesWeek_1*dfOut$spsWeek_1*dfOut$tpsWeek_1*(dfOut$rptWeek-dfOut$rptWeek_1)
    
    #avg
    dSesA <- (dSesB+dSesF)/2
    dConA <- (dConB+dConF)/2
    dRptA <- (dRptB+dRptF)/2
    
    dSes2A <- (dSes2B+dSes2F)/2
    dSpsA  <- (dSpsB+dSpsF)/2
    dTpsA  <- (dTpsB+dTpsF)/2
    dRpt2A <- (dRpt2B+dRpt2F)/2
    
    # Pct
    dSum <- dSesA + dConA + dRptA
    dSum1 <- dSes2A + dSpsA + dTpsA + dRpt2A
    
    dfOut$dSesPct <- (dSesA/dSum)
    dfOut$dConPct <- (dConA/dSum)
    dfOut$dRptPct <- (dRptA/dSum)
    
    dfOut$dSpsPct <- (dSpsA/dSum1)
    dfOut$dTpsPct <- (dTpsA/dSum1)
    
    #norm to amount
    dfOut$dSes <- dfOut$revDifWvW*dfOut$dSesPct
    dfOut$dCon <- dfOut$revDifWvW*dfOut$dConPct
    dfOut$dRpt <- dfOut$revDifWvW*dfOut$dRptPct
    dfOut$dSps <- dfOut$revDifWvW*dfOut$dSpsPct
    dfOut$dTps <- dfOut$revDifWvW*dfOut$dTpsPct
    
    #norm to Pct of total rev change
    dfOut$dSesPctWoW <- dfOut$revWoW*dfOut$dSesPct
    dfOut$dConPctWoW <- dfOut$revWoW*dfOut$dConPct
    dfOut$dRptPctWoW <- dfOut$revWoW*dfOut$dRptPct
    
    dfOut$dSpsPctWoW <- dfOut$revWoW*dfOut$dSpsPct
    dfOut$dTpsPctWoW <- dfOut$revWoW*dfOut$dTpsPct
    
    dfOut$flag <- 0
    
    control <- round(dfOut$revDifWvW - dfOut$dSes - dfOut$dCon - dfOut$dRpt, digits = 8)
    output[[length(output)+1]] <- list(0, paste("Succes. Control is ", control))
    output[[length(output)+1]] <- dfOut
  }
  output
}








#---- Weekly data ----

# Go through all selected markets (in settings) and test for revenue drops below threashold
# j is for control
j <- 0
for (market in marketsDF$market) {
  # Say hello
  print(market)
  
  # Make channels vector
  channels <- unique(gaData[gaData$market == market, ]$channelGrouping)
  channels <- c("allChannels", channels)
  #channels <- c("allChannels")
  # Products vector
  products <- c("allProducts", "flight", "other")
  
  for (channel in channels) {
    for (product in products) {
      # get output from function
      fnout <- getEffect(market, channel, product, gaData)
      
      # Add to our control
      j <- j + fnout[[1]][[1]]
      #If non valid return, print problem
      if(fnout[[1]][[1]] == 1) {
        print(paste(market, channel, product, fnout[[1]][[1]], fnout[[1]][[2]], sep=", "))
      }
      
      # Add up data
      if(exists("allPerfWeek")) {
        allPerfWeek <- rbind(allPerfWeek, fnout[[2]])
      } else {
        allPerfWeek <- fnout[[2]]
      }
    }
  }
  
  # Clean up
  rm(fnout)
  if(match(market,marketsDF$market) == length(marketsDF$market)) {
    if (j==0) { print("DONE: No warnings found") } else { print("Warnings found. Review. ")}
  }
}
rm(j, channel, channels, market, product, products)

# Sort markets to watch by weekly revenue drop
allPerfWeek <- allPerfWeek[allPerfWeek$flag == 0, ]
allPerfWeek <- allPerfWeek[order(allPerfWeek$market, allPerfWeek$product, allPerfWeek$channel, allPerfWeek$revWeek), ]
rownames(allPerfWeek) <- seq(length=nrow(allPerfWeek)) 
save(allPerfWeek, file = paste0(arguments$project_path, "Data/", arguments$brand, "-allPerfWeek.Rda"))



#---- Daily data ----

# Go through all selected markets (in settings) and test for revenue drops below threashold
# j is for control
j <- 0
for (market in marketsDF$market) {
  # Say hello
  print(market)
  
  # Make channels vector
  channels <- unique(gaData[gaData$market == market, ]$channelGrouping)
  channels <- c("allChannels", channels)
  #channels <- c("allChannels")
  # Products vector
  products <- c("allProducts", "flight", "other")
  
  for (channel in channels) {
    for (product in products) {
      # get output from function
      fnout <- getEffect(market, channel, product, gaData, c(endDate, endDate, (endDate-7), (endDate-7)))
      
      # Add to our control
      j <- j + fnout[[1]][[1]]
      #If non valid return, print problem
      if(fnout[[1]][[1]] == 1) {
        print(paste(market, channel, product, fnout[[1]][[1]], fnout[[1]][[2]], sep=", "))
      }
      
      # Add up data
      if(exists("allPerfDaily")) {
        allPerfDaily <- rbind(allPerfDaily, fnout[[2]])
      } else {
        allPerfDaily <- fnout[[2]]
      }
    }
  }
  
  # Clean up
  rm(fnout)
  if(match(market,marketsDF$market) == length(marketsDF$market)) {
    if (j==0) { print("DONE: No warnings found") } else { print("Warnings found. Review. ")}
  }
}
rm(j, channel, channels, market, product, products)

# Sort markets to watch by weekly revenue drop
allPerfDaily <- allPerfDaily[allPerfDaily$flag == 0, ]
allPerfDaily <- allPerfDaily[order(allPerfDaily$market, allPerfDaily$product, allPerfDaily$channel, allPerfDaily$revWeek), ]
rownames(allPerfDaily) <- seq(length=nrow(allPerfDaily)) 
save(allPerfDaily, file = paste0(arguments$project_path, "Data/", arguments$brand, "-allPerfDaily.Rda", sep=""))


#---- Make output and send mail ----

# Make output
# Save locally
outfileLoc = paste0(arguments$project_path, "Reports/", toupper(arguments$brand), "-DailyMarketReport-", Sys.Date(), ".pdf")
rmarkdown::render(input = paste(arguments$project_path, "5_CF_DailyMarketReport_v3.Rmd", sep=""), 
                  output_file = outfileLoc)

# Save on shared drive
outfileSh = paste(arguments$public_dir_path, "Brief Report/", toupper(arguments$brand), "-DailyMarketReport-", Sys.Date(), ".pdf", sep="")
file.copy(outfileLoc, outfileSh)


# Make Detailed report
# Save locally
outfileDLoc = paste(arguments$project_path, "Reports/", toupper(arguments$brand), "-DailyMarketReportDetailed-", Sys.Date(), ".pdf", sep="")
rmarkdown::render(input = paste(arguments$project_path, "6_CF_DailyMarketReportDetailed_v3.Rmd", sep=""), 
                  output_file = outfileDLoc)


# Save shared
outfileDSh = paste(arguments$public_dir_path, "Detailed Report/", toupper(arguments$brand), "-DailyMarketReport_Detailed-", Sys.Date(), ".pdf", sep="")
file.copy(outfileDLoc, outfileDSh)





# HOUSECLEANING!
rm(allPerfDaily, allPerfWeek, token)
rm(gaData, marketsDF)
rm(endDate, latestData, outfileDLoc, outfileDSh, outfileLoc, outfileSh, startDate, update_data, getEffect, threashold_gain, threashold_loss)

